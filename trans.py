def trans(razm: float, from_met: str, to: str):
    lst: list = ['ml', 'c', 'd', '', 'k']
    pos_f = 0
    pos_t = 0
    if from_met == 'k':
        pos_f = 6
    if to == 'k':
        pos_t = 6
    for i in range(len(lst)-1):
        if lst[i] == from_met:
            pos_f = i
        if lst[i] == to:
            pos_t = i
    return razm*pow(10, (pos_f - pos_t))


if __name__ == '__main__':
    print(trans(4, '', 'ml'))
